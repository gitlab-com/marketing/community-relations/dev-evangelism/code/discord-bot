# frozen_string_literal: true

require "discorb"
require "dotenv/load"  # Load environment variables from .env file.

require_relative "lib/event_manager"
require_relative "lib/member_counter"

client = Discorb::Client.new  # Create client for connecting to Discord

client.once :standby do
  puts "Logged in as #{client.user}"  # Prints username of logged in user
end

client.load_extension(EventManager)
client.load_extension(MemberCounter)

client.run ENV["TOKEN"]  # Starts client
