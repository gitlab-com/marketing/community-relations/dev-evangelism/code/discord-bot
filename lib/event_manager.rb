# frozen_string_literal: true

require "discorb"

class EventManager
  include Discorb::Extension

  slash_group("event", "Manage Events") do |group|
    group.slash("list", "List Current Events") do |interaction|
      ig = interaction.guild
      #require 'pry-byebug'; binding.pry
      if ig.scheduled_events.count > 0
        interaction.post("I found #{ig.scheduled_events.count} events, listing them now:", ephemeral: false)
        ig.scheduled_events.each do |event|
          message = interaction.channel.post("#{event.name}: #{event.start_at}").wait
        end
      else
        interaction.post("I didn't find any events :sob:", ephemeral: false)
      end
    end

#    group.group("subgroup", "Subcommand group") do |group|
#      group.slash("group_subcommand", "Command in Subcommand group") do |interaction|
#        # ...
#      end
#    end
  end
end
