# frozen_string_literal: true

require "discorb"

class MemberCounter
  include Discorb::Extension

  slash("membercount", "Count the members on the server") do |interaction|
    #require 'pry-byebug'; binding.pry
    interaction.post(interaction.guild.member_count, ephemeral: false)
  end
end
